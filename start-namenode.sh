#!/bin/bash

# start ssh server
/etc/init.d/ssh start

# format namenode
if [ ! -d /hdfs/volume1/name/current ]; then
	echo "Formatting namenode"
	$HADOOP_HOME/bin/hdfs namenode -format
else
	echo "It Appears this namenode is ready. Skipping format."
fi

# start namenode
$HADOOP_HOME/bin/hdfs --daemon start namenode

# check if namenode is running 
STATUS=$(jps)
if [[ $STATUS == *"NameNode"* ]]; then
  echo "NameNode started successfully"
  
  # keep container running
  tail -f /dev/null
fi

